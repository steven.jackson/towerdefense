package game;

import jig.ResourceManager;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.gui.MouseOverArea;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class StartUpState extends BasicGameState {

    MouseOverArea startGame;
    MouseOverArea leaderboard;

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException{
        container.setSoundOn(false);
        startGame = new MouseOverArea(container, ResourceManager.getImage(Game.START_GAME_RSC), (container.getWidth()/2)-131, 100);
        leaderboard = new MouseOverArea(container, ResourceManager.getImage(Game.LEADERBOARD_RSC), (container.getWidth()/2)-135, 200);
    }

    @Override
    public void enter(GameContainer container, StateBasedGame game){

    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException{
        Game tdg = (Game)game;
        Input input = container.getInput();
        g.drawString("Mouse Coordinates: (" + input.getMouseX() + ", " + input.getMouseY() + ")", 10, 30);
        if(startGame.isMouseOver()){
            g.drawImage(ResourceManager.getImage(Game.START_GAME_HOVER_RSC),(container.getWidth()/2)-131, 100);
        }else {g.drawImage(ResourceManager.getImage(Game.START_GAME_RSC), (container.getWidth()/2)-131,100);}
        if(leaderboard.isMouseOver()){
            g.drawImage(ResourceManager.getImage(Game.LEADERBOARD_HOVER_RSC),(container.getWidth()/2)-135, 200);
        }else {g.drawImage(ResourceManager.getImage(Game.LEADERBOARD_RSC), (container.getWidth()/2)-135,200);}
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException{
        Input input = container.getInput();
        Game tdg = (Game)game;
        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)){
            if(startGame.isMouseOver()) tdg.enterState(Game.PLAYINGSTATE);
            else if (leaderboard.isMouseOver()) tdg.enterState(Game.GAMEOVERSTATE);
        }
    }

    @Override
    public int getID(){
        return Game.STARTUPSTATE;
    }
}

package game;

import jig.ResourceManager;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;


public class GameOverState extends BasicGameState {
    LinkedList<ScoreCard> leaderboard;
    ScoreCard newScore;
    int nameIndex;
    int timer;

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException{
        leaderboard = new LinkedList<>();
        newScore = null;
        nameIndex = 0;
        timer = 800;
        try {
            loadLeaderboard();
        } catch (SlickException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void enter(GameContainer container, StateBasedGame game) {
        if (PlayingState.gamePlayed) {
            PlayingState.resetGameBoard();
            EnemyWaveState.wave = 0;
            EnemyWaveState.velocityMultiplier = 1;
            EnemyWaveState.fastForwarding = false;
            newScore = new ScoreCard("___", PlayingState.getScore());
            leaderboard.add(newScore);
            Collections.sort(leaderboard);
            if (leaderboard.size() > 10) leaderboard.removeLast();
            if (!leaderboard.contains(newScore)) newScore = null;
            try {
                saveLeaderboard();
            } catch (SlickException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException{
        Game tdg = (Game)game;
        g.drawImage(ResourceManager.getImage(Game.LEADERBOARD_RSC), (((Game) game).ScreenWidth/2) - 135, 100);
        g.setColor(Color.decode("#20b21e"));
        for (ScoreCard scoreCard : leaderboard){
            g.drawString(scoreCard.name + ": " + scoreCard.score, 380, (leaderboard.indexOf(scoreCard) * 30) + 200);
        }
        if (newScore != null && timer < 400) g.drawLine(380 + (nameIndex * 9), (leaderboard.indexOf(newScore) * 30) + 200, 380 + (nameIndex * 9), (leaderboard.indexOf(newScore) * 30) + 214);

    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException{
        Input input = container.getInput();
        Game tdg = (Game)game;
        timer -= delta;
        if (timer < 0) timer = 800;
        if (input.isKeyPressed(Input.KEY_ESCAPE)){
            PlayingState.setScore(0);
            PlayingState.gamePlayed = false;
            tdg.enterState(Game.STARTUPSTATE);
        }
    }

    @Override
    public int getID(){
        return Game.GAMEOVERSTATE;
    }

    @Override
    public void keyPressed(int key, char c){
        System.out.println("key: " + key + " c: " + c);
        if (Character.isLetter(c)) System.out.println("True");
        if (newScore != null && nameIndex <= 3){
            if (Character.isLetter(c) && nameIndex < 3) {
                StringBuilder newName = new StringBuilder(newScore.name);
                newName.setCharAt(nameIndex, c);
                newScore.name = newName.toString();
                nameIndex++;
            }
            else if (key == Input.KEY_BACK && nameIndex > 0){
                StringBuilder newName = new StringBuilder(newScore.name);
                newName.setCharAt(nameIndex - 1, '_');
                newScore.name = newName.toString();
                nameIndex--;
            }
            else if (key == Input.KEY_ENTER){
                newScore = null;
            }
            try {
                saveLeaderboard();
            } catch (SlickException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadLeaderboard() throws SlickException, IOException {
        SavedState muffin = new SavedState("muffin");
        muffin.load();
        String name;
        int i = 0;
        while ((name = muffin.getString("name" + i, null)) != null){
            leaderboard.add(new ScoreCard(name, (int)muffin.getNumber("score"+ i)));
            i++;
        }
    }

    private void saveLeaderboard() throws SlickException, IOException {
        SavedState muffin = new SavedState("muffin");
        for (ScoreCard scoreCard : leaderboard){
            System.out.println(leaderboard.indexOf(scoreCard));
            muffin.setString("name" + leaderboard.indexOf(scoreCard), scoreCard.name);
            muffin.setNumber("score" + leaderboard.indexOf(scoreCard), scoreCard.score);
        }
        muffin.save();
    }

    private class ScoreCard implements Comparable<ScoreCard>{
        String name;
        int score;

        ScoreCard(String n, int s){
            name = n;
            score = s;
        }

        @Override
        public int compareTo(ScoreCard s){
            return -(this.score - s.score);
        }
    }
}

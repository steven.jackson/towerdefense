package game;

import jig.ResourceManager;
import jig.Vector;
import org.newdawn.slick.*;
import org.newdawn.slick.gui.MouseOverArea;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.util.LinkedList;
import java.util.PriorityQueue;

public class PlayingState extends BasicGameState {

    MouseOverArea gameBoardMap;
    MouseOverArea[] menuTower;
    MouseOverArea startWave;
    MouseOverArea speedUpgrade;
    public static int towerSelected;
    public static String[] towerRSC;
    public static String[] towerSelectedRSC;
    public static LinkedList<DefenseTower> defenseTower;
    public static int numDefenseTowers;
    public static int[][] gameTile = {
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    };
    public static Coordinate[] start;
    public static Node[][] graph;
    private static int currency;
    private static int score;
    private static int health;
    public static DefenseTower towerToUpgrade;
    public static boolean gamePlayed;
    public static Gun[] menuGun;
    public static Gun[] upgradeGun;
    private boolean tileAvailability;
    Coordinate mouseTile;

    @Override
    public void init(GameContainer container, StateBasedGame game){
        tileAvailability = false;
        menuGun = new Gun[3];
        upgradeGun = new Gun[3];
        gamePlayed = false;
        towerToUpgrade = null;
        currency = 100;
        score = 0;
        health = 100;
        towerRSC = new String[3];
        towerSelectedRSC = new String[3];
        menuTower = new MouseOverArea[3];
        towerRSC[0] = Game.TOWER_1_RSC;
        towerRSC[1] = Game.TOWER_2_RSC;
        towerRSC[2] = Game.TOWER_3_RSC;
        towerSelectedRSC[0] = Game.TOWER_1_SELECTED_RSC;
        towerSelectedRSC[1] = Game.TOWER_2_SELECTED_RSC;
        towerSelectedRSC[2] = Game.TOWER_3_SELECTED_RSC;
        gameBoardMap = new MouseOverArea(container, ResourceManager.getImage(Game.GAME_BOARD_MAP_RSC), 100, 0);
        for(int i = 0; i < 3; i++) {
            menuTower[i] = new MouseOverArea(container, ResourceManager.getImage(towerRSC[i]), 25, 25 + (i * 80));
        }
        for(int i = 0; i < 3; i++){
            menuGun[i] = new Gun(50, 50 + (i * 80), 1, 0, i);
        }
        for (int i = 0; i < 3; i++){
            upgradeGun[i] = new Gun(300, 550, 1, 0, i);
        }
        startWave = new MouseOverArea(container, ResourceManager.getImage(Game.START_WAVE_RSC), 25, 538);
        speedUpgrade = new MouseOverArea(container, ResourceManager.getImage(Game.SPEED_UPGRADE_RSC), 355, 525);
        towerSelected = 0;
        defenseTower = new LinkedList<>();
        numDefenseTowers = 0;
        start = new Coordinate[2];
        start[0] = new Coordinate(0, 4);
        start[1] = new Coordinate(0, 5);
        graph = new Node[14][10];
        initGraph();
    }

    @Override
    public void enter(GameContainer container, StateBasedGame game){
        gamePlayed = true;
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g){
        Game tdg = (Game)game;
        g.drawImage(ResourceManager.getImage(Game.GAME_BOARD_RSC), 0, 0);
        g.drawImage(ResourceManager.getImage(Game.GAME_BOARD_MAP_RSC), 100, 0);
        printMenu(g);
        for(int i = 0; i < 3; i++) {
            if (towerSelected == i) g.drawImage(ResourceManager.getImage(towerSelectedRSC[i]), 25, 25 + (i * 80));
            else g.drawImage(ResourceManager.getImage(towerRSC[i]), 25, 25 + (i * 80));
            menuGun[i].render(g);
            g.drawString("Cost: $" + ((i + 1) * 25), 10, 80 + (i * 80));
        }
        g.drawImage(ResourceManager.getImage(Game.START_WAVE_RSC), 25, 538);
        for (DefenseTower tower : defenseTower){
            g.drawImage(tower.image, tower.coordinate.x, tower.coordinate.y);
            tower.gun.render(g);
        }
        if (towerToUpgrade != null){
            g.drawImage(ResourceManager.getImage(Game.TOWER_SELECTED_BORDER_RSC), 250, 510);
            g.drawImage(towerToUpgrade.image, 275, 525);
            g.drawImage(ResourceManager.getImage(Game.SPEED_UPGRADE_RSC), 355, 525);
            upgradeGun[towerToUpgrade.towerType].render(g);
            if (towerToUpgrade.isSpeedUpgraded()){
                g.drawString("Purchased!", 340, 560);
            } else g.drawString("Cost: $20", 340, 560);
        }
        if (tileAvailability){
            g.drawImage(ResourceManager.getImage(Game.AVAILABLE_RSC), mouseTile.x, mouseTile.y);
        }
        else if(gameTile[mouseTile.y/50][(mouseTile.x-100)/50] == 0){
            g.drawImage(ResourceManager.getImage(Game.NOT_AVAILABLE_RSC), mouseTile.x, mouseTile.y);
        }

            g.setColor(Color.black);
//        printGraph(g);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException{
        Input input = container.getInput();
        Game tdg = (Game)game;
        if (gameBoardMap.isMouseOver()){
            setTileAvailability(input);
        }
        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)){
            if(gameBoardMap.isMouseOver()){
                if (placeTower(input, container));
                else {
                    for (DefenseTower tower : defenseTower) {
                        if (tower.clickable.isMouseOver()) {
                            towerToUpgrade = tower;
                        }
                    }
                }
            }else {
//                printDirection();
                for (int i = 0; i < 3; i++) {
                    if (menuTower[i].isMouseOver()) {
                        towerSelected = i;
                        System.out.println("Tower " + i + " selected.");
//                        for (int j = 0; j < 3; j++) if (i != j) towerSelected[i] = false;
                    }
                }
                if(startWave.isMouseOver()){
                    findPath();
                    towerToUpgrade = null;
                    tdg.enterState(Game.ENEMYWAVESTATE);
                }
                else if (speedUpgrade.isMouseOver()){
                    if (currency >= 20) {
                        currency -= 20;
                        towerToUpgrade.setSpeedUpgraded(true);
                        towerToUpgrade.setRateOfFire(1.5f);
                    }
                }
            }
        }
    }

    @Override
    public int getID(){
        return Game.PLAYINGSTATE;
    }

    public static void resetGameBoard(){
        defenseTower.clear();
        currency = 100;
        towerSelected = 0;
        health = 100;
        for (int i = 1; i < 13; i++){
            for (int j = 1; j < 9; j++){
                gameTile[j][i] = 0;
            }
        }
    }

    private void setTileAvailability(Input input){
        mouseTile = getMouseTile(input);
        if (currency >= ((towerSelected + 1) * 25)){
            if (isPlaceable(mouseTile.x, mouseTile.y)) {
                gameTile[mouseTile.y / 50][(mouseTile.x - 100) / 50] = 1;
                if (findPath()) {
                    tileAvailability = true;
                    gameTile[mouseTile.y / 50][(mouseTile.x - 100) / 50] = 0;
                    return;
                }
                gameTile[mouseTile.y / 50][(mouseTile.x - 100) / 50] = 0;
            }

        }
        tileAvailability = false;
    }


    public static int getHealth(){
        return health;
    }

    public static void printMenu(Graphics g){
        g.setColor(Color.black);
        g.fillRect(0, 0, 100, 600);
        g.fillRect(100, 500, 700, 100);
        g.drawImage(ResourceManager.getImage(Game.HEALTH_BAR_RSC), 25, 325);
        int healthPercent = 163 - ((163 * health)/100);
        g.setColor(Color.decode("#2aa529"));
        g.fillRect(31, 331 + healthPercent, 38, 163 - healthPercent);
        g.setColor(Color.decode("#3adb39"));
        g.drawRect(31, 331 + healthPercent, 38, 163 - healthPercent);
        g.setColor(Color.decode("#20b21e"));
        g.drawString("Health", 22, 300);
        g.drawString("Gold: $" + currency, 100f, 520f);
        g.drawString("Score: " + score, 100f, 560f);
    }

    public static void addCurrency(int earnings){
        currency += earnings;
    }

    public static void addScore(int scorePoints){
        score += scorePoints;
    }

    public static int getScore(){
        return score;
    }

    public static void setScore(int s){
        score = s;
    }

    public static void reduceLife(int damage){
        health -= damage;
    }

    public boolean placeTower(Input input, GameContainer container){
        if (currency >= ((towerSelected + 1) * 25)){
            Coordinate c = getMouseTile(input);
            if (isPlaceable(c.x, c.y)) {
                gameTile[c.y / 50][(c.x - 100) / 50] = 1;
                if (findPath()) {
                    towerToUpgrade = null;
                    currency -= ((towerSelected + 1) * 25);
                    defenseTower.add(new DefenseTower(c, towerSelected, container));
                    numDefenseTowers++;
                    return true;
                } else {
                    gameTile[c.y / 50][(c.x - 100) / 50] = 0;
                }
            }

        }
        return false;
    }

    public Coordinate getMouseTile(Input input){
        int x = input.getMouseX();
        int y = input.getMouseY();
        x -= (x%50);
        y -= (y%50);
//        System.out.println("Coordinates: (" + x + ", " + y + ")");
        Coordinate c = new Coordinate(x, y);
        return c;
    }

    public static Vector getTileDirection(int x, int y){
        x -= x%50; x = (x - 100)/50;
        y -= y%50; y = y/50;
        if (x >= 0 && x < 14 && y >= 0 && y < 10) {
            Vector v = graph[x][y].pi;
            return v;
        } else return null;
    }

    public boolean isPlaceable(int x, int y){
        if(gameTile[y/50][(x-100)/50] == 0){
            if(findPath()){
                return true;
            }
        }
        return false;
    };

    public boolean findPath(){
        initGraph();
//        LinkedList<Node> Q = new LinkedList<>();
        PriorityQueue<Node> Q = new PriorityQueue<>();
        Q.add(graph[0][4]);
        Q.add(graph[0][5]);
        Node minNode;
        boolean dest1 = false, dest2 = false;
        while (!Q.isEmpty()){
            minNode = Q.remove();
            int x, y;
            x = minNode.position.x; y = minNode.position.y - 1;
            if (isTraversable(x, y)) {
                if (gameTile[y][x] == 0 && graph[x][y].d == Double.POSITIVE_INFINITY) {
                    graph[x][y].d = minNode.d + 1;
                    graph[x][y].setPi(0, .05f);
                    Q.add(graph[x][y]);
                }
            }
            x = minNode.position.x; y = minNode.position.y + 1;
            if (isTraversable(x, y)) {
                if (gameTile[y][x] == 0 && graph[x][y].d == Double.POSITIVE_INFINITY) {
                    graph[x][y].d = minNode.d + 1;
                    graph[x][y].setPi(0, -.05f);
                    Q.add(graph[x][y]);
                }
            }
            x = minNode.position.x - 1; y = minNode.position.y;
            if (isTraversable(x, y)) {
                if (gameTile[y][x] == 0 && graph[x][y].d == Double.POSITIVE_INFINITY) {
                    graph[x][y].d = minNode.d + 1;
                    graph[x][y].setPi(.05f, 0);
                    Q.add(graph[x][y]);
                }
            }
            x = minNode.position.x + 1; y = minNode.position.y;
            if (isTraversable(x, y)) {
                if (gameTile[y][x] == 0 && graph[x][y].d == Double.POSITIVE_INFINITY) {
                    graph[x][y].d = minNode.d + 1;
                    graph[x][y].setPi(-.05f, 0);
                    Q.add(graph[x][y]);
                }
            }
            if (minNode.position.x == 13 && minNode.position.y == 4) dest1 = true;
            if (minNode.position.x == 13 && minNode.position.y == 5) dest2 = true;
            if (dest1 && dest2) return true;
        }
        return false;
    }

    public void printQ(LinkedList<Node> Q){
        for (Node node : Q){
            System.out.print(node.d + ", ");
        }
    }

    public boolean isTraversable(int x, int y){
        if (x > 0 && x < 13 && y < 9 && y > 0) if (gameTile[y][x] == 0) return true;
        if (x == 0 && (y == 4 || y == 5)) return true;
        if (x == 13 && (y == 4 || y == 5)) return true;
        return false;
    }

    public void printGraph(Graphics g){
        for (int x = 0; x < 14; x++){
            for (int y = 0; y < 10; y++){
                if (graph[x][y].d != Double.POSITIVE_INFINITY) g.drawString(Double.toString(graph[x][y].d), (x * 50) + 100, y * 50);
            }
        }
    }

    public void printDirection(){
        for (int y = 0; y < 10; y++){
            for (int x = 0; x < 14; x++){
                System.out.print(y + x);
            }
            System.out.println("");
        }
    }

    public void initGraph(){
        for (int y = 0; y < 10; y++){
            for (int x = 0; x < 14; x++){
                graph[x][y]  = new Node(Double.POSITIVE_INFINITY, null, x, y);
            }
        }
        graph[start[0].x][start[0].y].d = 0;
        graph[start[0].x][start[0].y].setPi(-.05f, 0);
        graph[start[1].x][start[1].y].d = 0;
        graph[start[1].x][start[1].y].setPi(-.05f, 0);
    }

    public class Coordinate {
        int x;
        int y;

        Coordinate(int x, int y){
            this.x = x;
            this.y = y;
        }
    }

    public class Node implements Comparable<Node>{
        double d;
        Vector pi;
        Coordinate position;

        Node(double cost, Vector direction, int xPos, int yPos){
            d = cost;
            pi = direction;
            position = new Coordinate(xPos, yPos);
        }

        private void setPi(float vx, float vy){
            pi = new Vector(vx, vy);
        }

        @Override
        public int compareTo(Node o) {
            return (int)(this.d - o.d);
        }
    }
}

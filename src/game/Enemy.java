package game;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class Enemy extends Entity {
    private int life;
    private Vector velocity;
    private float velocityMultiplier;
    private int type;

    Enemy(int x, int y, final float vx, final float vy, int lifeScaler, int enemyType){
        super((x * 50) + 125, (y * 50) + 25);
        velocityMultiplier = 1f;
        life = (((lifeScaler - 1) * 15) + 10) + (lifeScaler * 4) * enemyType;
        type = enemyType;
        if (enemyType == 1) {
            addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_1_LEFT_RSC));
            scale(.38f);
        }
        else if (enemyType == 2) {
            addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_2_LEFT_RSC));
            scale(.38f);
        }
        else if (enemyType == 3) {
            addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_3_LEFT_RSC));
            scale(.42f);
        }
        velocity = new Vector(vx, vy);
    }

    public void fastForward(float multiplier){
        velocityMultiplier = multiplier;
        velocity = velocity.scale(multiplier);
    }

    public void disableFastForward(float multiplier){
        velocityMultiplier = 1f;
        velocity = velocity.scale(multiplier);
    }

    public void reduceLife(int h){
        System.out.println("Enemy Life: " + this.life);
        this.life = life - h;
    }

    public int getLife(){
        return this.life;
    }

    public int getType(){
        return type;
    }

    public void update(final int delta){
        float x1 = this.getX()%50, y1 = this.getY()%50;
        translate(velocity.scale(delta));
        float x2 = this.getX()%50, y2 = this.getY()%50;
        if (velocity.getX() > 0 && ((x1 < 25 && 25 < x2) || x2 == 25)){
            if (x2 != 25) {
                this.setX(getX() - (x2 % 25));
            }
            changeDirection();
        }
        else if (velocity.getX() < 0 && ((x2 < 25 && 25 < x1) || x2 == 25)){
            if (x2 != 25) {
                this.setX(this.getX() + (25 - (x2 % 25)));
            }
            changeDirection();
        }
        else if (velocity.getY() > 0 && ((y1 < 25 && 25 < y2) || y2 == 25)){
            if (y2 != 25) {
                this.setY(getY() - (y2 % 25));
            }
            changeDirection();
        }
        else if (velocity.getY() < 0 && ((y2 < 25 && 25 < y1) || y2 == 25)){
            if (y2 != 25) {
                this.setY(this.getY() + (25 - (y2 % 25)));
            }
            changeDirection();
        }
    }

    private void changeDirection() {
        Vector newVelocity = PlayingState.getTileDirection((int) this.getX(), (int) this.getY());
        if (newVelocity != null && velocity.getRotation() != newVelocity.getRotation()) {

             if (velocity.getRotation() == -90){
                if (type == 1) removeImage(ResourceManager.getImage(Game.TANK_1_UP_RSC));
                else if (type == 2) removeImage(ResourceManager.getImage(Game.TANK_2_UP_RSC));
                else if (type == 3) removeImage(ResourceManager.getImage(Game.TANK_3_UP_RSC));
            }
            else if (velocity.getRotation() == 0){
                if (type == 1) removeImage(ResourceManager.getImage(Game.TANK_1_RIGHT_RSC));
                else if (type == 2) removeImage(ResourceManager.getImage(Game.TANK_2_RIGHT_RSC));
                else if (type == 3) removeImage(ResourceManager.getImage(Game.TANK_3_RIGHT_RSC));
            }
            else if (velocity.getRotation() == 90){
                if (type == 1) removeImage(ResourceManager.getImage(Game.TANK_1_DOWN_RSC));
                else if (type == 2) removeImage(ResourceManager.getImage(Game.TANK_2_DOWN_RSC));
                else if (type == 3) removeImage(ResourceManager.getImage(Game.TANK_3_DOWN_RSC));
            }
            else if (velocity.getRotation() == 180){
                if (type == 1) removeImage(ResourceManager.getImage(Game.TANK_1_LEFT_RSC));
                else if (type == 2) removeImage(ResourceManager.getImage(Game.TANK_2_LEFT_RSC));
                else if (type == 3) removeImage(ResourceManager.getImage(Game.TANK_3_LEFT_RSC));
            }
            if (newVelocity.getRotation() == -90){
                if (type == 1) addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_1_UP_RSC));
                else if (type == 2) addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_2_UP_RSC));
                else if (type == 3) addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_3_UP_RSC));
            }
            else if (newVelocity.getRotation() == 0){
                if (type == 1) addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_1_RIGHT_RSC));
                else if (type == 2) addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_2_RIGHT_RSC));
                else if (type == 3) addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_3_RIGHT_RSC));
            }
            else if (newVelocity.getRotation() == 90){
                if (type == 1) addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_1_DOWN_RSC));
                else if (type == 2) addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_2_DOWN_RSC));
                else if (type == 3) addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_3_DOWN_RSC));
            }
            else if (newVelocity.getRotation() == 180){
                if (type == 1) addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_1_LEFT_RSC));
                else if (type == 2) addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_2_LEFT_RSC));
                else if (type == 3) addImageWithBoundingBox(ResourceManager.getImage(Game.TANK_3_LEFT_RSC));
            }
            velocity = newVelocity.scale(velocityMultiplier);
        }
    }

    public Vector getVelocity(){
        return velocity;
    }
}

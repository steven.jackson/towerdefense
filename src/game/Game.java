package game;

import jig.Entity;
import jig.ResourceManager;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class Game extends StateBasedGame {

    public static final int STARTUPSTATE = 0;
    public static final int PLAYINGSTATE = 1;
    public static final int ENEMYWAVESTATE = 2;
    public static final int GAMEOVERSTATE = 3;

    public static final String GAME_BOARD_RSC = "game/resource/gameboard.png";
    public static final String GAME_BOARD_MAP_RSC = "game/resource/mapwithborders.png";
    public static final String START_GAME_RSC = "game/resource/startgame.png";
    public static final String START_GAME_HOVER_RSC = "game/resource/startgamehover.png";
    public static final String LEADERBOARD_RSC = "game/resource/leaderboard.png";
    public static final String LEADERBOARD_HOVER_RSC = "game/resource/leaderboardhover.png";
    public static final String TOWER_1_RSC = "game/resource/tower1.png";
    public static final String TOWER_2_RSC = "game/resource/tower2.png";
    public static final String TOWER_3_RSC = "game/resource/tower3.png";
    public static final String TOWER_1_GUN_RSC = "game/resource/tower1gun.png";
    public static final String TOWER_2_GUN_RSC = "game/resource/tower2gun.png";
    public static final String TOWER_3_GUN_RSC = "game/resource/tower3gun.png";
    public static final String TOWER_1_SELECTED_RSC = "game/resource/tower1selected.png";
    public static final String TOWER_2_SELECTED_RSC = "game/resource/tower2selected.png";
    public static final String TOWER_3_SELECTED_RSC = "game/resource/tower3selected.png";
    public static final String TANK_1_LEFT_RSC = "game/resource/tank1left.png";
    public static final String TANK_2_LEFT_RSC = "game/resource/tank2left.png";
    public static final String TANK_3_LEFT_RSC = "game/resource/tank3left.png";
    public static final String TANK_1_UP_RSC = "game/resource/tank1up.png";
    public static final String TANK_2_UP_RSC = "game/resource/tank2up.png";
    public static final String TANK_3_UP_RSC = "game/resource/tank3up.png";
    public static final String TANK_1_RIGHT_RSC = "game/resource/tank1right.png";
    public static final String TANK_2_RIGHT_RSC = "game/resource/tank2right.png";
    public static final String TANK_3_RIGHT_RSC = "game/resource/tank3right.png";
    public static final String TANK_1_DOWN_RSC = "game/resource/tank1down.png";
    public static final String TANK_2_DOWN_RSC = "game/resource/tank2down.png";
    public static final String TANK_3_DOWN_RSC = "game/resource/tank3down.png";
    public static final String START_WAVE_RSC = "game/resource/startwave.png";
    public static final String FAST_FORWARD_RSC = "game/resource/fastforward.png";
    public static final String DISABLE_FAST_FORWARD_RSC = "game/resource/disablefastforward.png";
    public static final String BULLET_1_RSC = "game/resource/bullet1.png";
    public static final String HEALTH_BAR_RSC = "game/resource/lifebar.png";
    public static final String SPEED_UPGRADE_RSC = "game/resource/speedupgrade.png";
    public static final String TOWER_SELECTED_BORDER_RSC = "game/resource/towerselectedborder.png";
    public static final String AVAILABLE_RSC = "game/resource/tileavailable.png";
    public static final String NOT_AVAILABLE_RSC = "game/resource/tilenotavailable.png";


    public static final String LEADERBOARD = "game/resource/leaderboard.txt";

    public final int ScreenWidth;
    public final int ScreenHeight;

    /* Load resources */

    public Game(String title, int width, int height){
        super(title);
        ScreenHeight = height;
        ScreenWidth = width;
        Entity.setCoarseGrainedCollisionBoundary(Entity.CIRCLE);
    }

    @Override
    public void initStatesList(GameContainer container) throws SlickException {
        addState(new StartUpState());
        addState(new GameOverState());
        addState(new PlayingState());
        addState(new EnemyWaveState());

        ResourceManager.loadImage(GAME_BOARD_RSC);
        ResourceManager.loadImage(GAME_BOARD_MAP_RSC);
        ResourceManager.loadImage(START_GAME_RSC);
        ResourceManager.loadImage(START_GAME_HOVER_RSC);
        ResourceManager.loadImage(LEADERBOARD_RSC);
        ResourceManager.loadImage(LEADERBOARD_HOVER_RSC);
        ResourceManager.loadImage(TOWER_1_RSC);
        ResourceManager.loadImage(TOWER_2_RSC);
        ResourceManager.loadImage(TOWER_3_RSC);
        ResourceManager.loadImage(TOWER_1_GUN_RSC);
        ResourceManager.loadImage(TOWER_2_GUN_RSC);
        ResourceManager.loadImage(TOWER_3_GUN_RSC);
        ResourceManager.loadImage(TOWER_1_SELECTED_RSC);
        ResourceManager.loadImage(TOWER_2_SELECTED_RSC);
        ResourceManager.loadImage(TOWER_3_SELECTED_RSC);
        ResourceManager.loadImage(TANK_1_LEFT_RSC);
        ResourceManager.loadImage(TANK_2_LEFT_RSC);
        ResourceManager.loadImage(TANK_3_LEFT_RSC);
        ResourceManager.loadImage(TANK_1_UP_RSC);
        ResourceManager.loadImage(TANK_2_UP_RSC);
        ResourceManager.loadImage(TANK_3_UP_RSC);
        ResourceManager.loadImage(TANK_1_RIGHT_RSC);
        ResourceManager.loadImage(TANK_2_RIGHT_RSC);
        ResourceManager.loadImage(TANK_3_RIGHT_RSC);
        ResourceManager.loadImage(TANK_1_DOWN_RSC);
        ResourceManager.loadImage(TANK_2_DOWN_RSC);
        ResourceManager.loadImage(TANK_3_DOWN_RSC);
        ResourceManager.loadImage(START_WAVE_RSC);
        ResourceManager.loadImage(FAST_FORWARD_RSC);
        ResourceManager.loadImage(DISABLE_FAST_FORWARD_RSC);
        ResourceManager.loadImage(BULLET_1_RSC);
        ResourceManager.loadImage(HEALTH_BAR_RSC);
        ResourceManager.loadImage(SPEED_UPGRADE_RSC);
        ResourceManager.loadImage(TOWER_SELECTED_BORDER_RSC);
        ResourceManager.loadImage(AVAILABLE_RSC);
        ResourceManager.loadImage(NOT_AVAILABLE_RSC);
    }

    public static void main(String[] args){
        AppGameContainer app;
        try{
            app = new AppGameContainer(new Game("Tower Defense", 800, 600));
            app.setDisplayMode(800, 600, false);
            app.setVSync(true);
            app.start();
        } catch(SlickException e){
            e.printStackTrace();
        }
    }
}

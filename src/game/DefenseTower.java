package game;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.gui.MouseOverArea;

public class DefenseTower {
    PlayingState.Coordinate coordinate;
    Image image;
    Vector position;
    int towerType;
    int timeTillShot;
    boolean reloading;
    MouseOverArea clickable;
    double rateOfFire;
    boolean speedUpgraded;
    Gun gun;

    DefenseTower(PlayingState.Coordinate c, int type, GameContainer container){
        gun = new Gun(c.x + 25, c.y + 25, 1, 0, type);
        speedUpgraded = false;
        rateOfFire = 1f;
        reloading = false;
        timeTillShot = (int)(400/rateOfFire);
        towerType = type;
        coordinate = c;
        position = new Vector(c.x, c.y);
        PlayingState.gameTile[c.y /50][(c.x - 100)/50] = 1;
//        addImage(ResourceManager.getImage(PlayingState.towerRSC[type]));
//        scale(.7f);
        image = ResourceManager.getImage(PlayingState.towerRSC[type]);
        clickable = new MouseOverArea(container, image, c.x, c.y);
    }

    public void update(final int delta){
        if (reloading){
            timeTillShot -= delta;
            if (timeTillShot < 0){
                reloading = false;
                timeTillShot = (int)(400/rateOfFire);
            }
        }
    }

    public void setSpeedUpgraded(boolean b){
        speedUpgraded = b;
    }

    public boolean isSpeedUpgraded(){
        return speedUpgraded;
    }

    public void setRateOfFire(float newRateOfFire){
        rateOfFire = newRateOfFire;
    }

    public Vector getPosition(){
        return new Vector(position.getX() + 25, position.getY() + 25);
    }
}

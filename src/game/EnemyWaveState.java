package game;

import jig.ResourceManager;
import jig.Vector;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.gui.MouseOverArea;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.util.LinkedList;

public class EnemyWaveState extends BasicGameState {

    MouseOverArea fastForward;
    public static boolean fastForwarding;
    public static int wave;
    LinkedList<Enemy> enemies;
    int numEnemies;
    LinkedList<Bullet> bullets;
    public static String[] bulletRSC;
    public static float velocityMultiplier;

    @Override
    public void init(GameContainer container, StateBasedGame game){
        velocityMultiplier = 1f;
        wave = 0;
        bullets = new LinkedList<>();
        enemies = new LinkedList<>();
        bulletRSC = new String[3];
        bulletRSC[0] = Game.BULLET_1_RSC;
        bulletRSC[1] = Game.BULLET_1_RSC;
        bulletRSC[2] = Game.BULLET_1_RSC;
        fastForwarding = false;
        fastForward = new MouseOverArea(container, ResourceManager.getImage(Game.FAST_FORWARD_RSC), 25, 525);
    }

    @Override
    public void enter(GameContainer container, StateBasedGame game){
        wave++;
        numEnemies = wave * 2;
        int i;
        for (i = 0; i < numEnemies; i++){
            enemies.add(new Enemy(14 +  (2 * i), 4, -.05f, 0f, wave, 1));
        }
        int j;
        for (j = i; j < (numEnemies + (i/2)); j++){
            enemies.add(new Enemy(14 + (2 * j), 4, -.05f, 0f, wave, 2));
        }
        for (int k = j; k < (numEnemies + (j/2)); k++){
            enemies.add(new Enemy(14 + (2 * k), 4, -.05f, 0f, wave, 3));
        }
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g){
        Game tdg = (Game)game;
        g.drawImage(ResourceManager.getImage(Game.GAME_BOARD_RSC), 0, 0);
        g.drawImage(ResourceManager.getImage(Game.GAME_BOARD_MAP_RSC), 100, 0);
        for (DefenseTower tower : PlayingState.defenseTower){
            g.drawImage(tower.image, tower.coordinate.x, tower.coordinate.y);
        }
        for (Enemy enemy : enemies){
            enemy.render(g);
        }
        for (Bullet bullet : bullets){
            bullet.render(g);
        }
        for (DefenseTower tower : PlayingState.defenseTower){
            tower.gun.render(g);
        }
        PlayingState.printMenu(g);
        for(int i = 0; i < 3; i++) {
            if (PlayingState.towerSelected == i) g.drawImage(ResourceManager.getImage(PlayingState.towerSelectedRSC[i]), 25, 25 + (i * 80));
            else g.drawImage(ResourceManager.getImage(PlayingState.towerRSC[i]), 25, 25 + (i * 80));
            PlayingState.menuGun[i].render(g);
        }
        if (fastForwarding){ g.drawImage(ResourceManager.getImage(Game.DISABLE_FAST_FORWARD_RSC), 25, 525);
        } else g.drawImage(ResourceManager.getImage(Game.FAST_FORWARD_RSC), 25, 525);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta){
        Input input = container.getInput();
        Game tdg = (Game)game;
        if (input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
            if (fastForward.isMouseOver()) {
                if (!fastForwarding) {
                    enableFastForward();
                } else disableFastForward();
            }
        }
        for (Enemy enemy : enemies) enemy.update(delta);
        for (Bullet bullet : bullets) bullet.update(delta);
        createBullets(delta);
        detectCollision();
        if (enemies.isEmpty()){
            bullets.clear();
            if(fastForwarding) disableFastForward();
            tdg.enterState(Game.PLAYINGSTATE);
        }
        damagePlayer(tdg);
    }

    private void enableFastForward(){
        fastForwarding = true;
        float multiplier = 4f;
        for (Enemy enemy : enemies) enemy.fastForward(multiplier);
        for (DefenseTower defenseTower : PlayingState.defenseTower) defenseTower.setRateOfFire(multiplier);
        velocityMultiplier = 4f;
        for (Bullet bullet : bullets) bullet.fastForward(multiplier);
    }

    private void disableFastForward(){
        fastForwarding = false;
        float multiplier = .25f;
        for (Enemy enemy : enemies) enemy.disableFastForward(multiplier);
        for (DefenseTower defenseTower : PlayingState.defenseTower) defenseTower.setRateOfFire(1f);
        velocityMultiplier = 1f;
        for (Bullet bullet : bullets) bullet.disableFastForward(multiplier);
    }

    private void damagePlayer(Game tdg){
        LinkedList<Enemy> enemiesToRemove = new LinkedList<>();
        for (Enemy enemy : enemies){
            if (enemy.getCoarseGrainedMaxX() < 100){
                PlayingState.reduceLife(enemy.getType() * 10);
                if (PlayingState.getHealth() <= 0){
                    PlayingState.defenseTower.clear();
                    enemies.clear();
                    bullets.clear();
                    tdg.enterState(Game.GAMEOVERSTATE);
                }
                enemiesToRemove.add(enemy);
            }
        }
        for (Enemy enemy : enemiesToRemove){
            enemies.remove(enemy);
        }
    }

    private void detectCollision(){
        LinkedList<Bullet> bulletsToRemove = new LinkedList<>();
        LinkedList<Enemy> enemiesToRemove = new LinkedList<>();
        for (Bullet bullet : bullets){
            for (Enemy enemy : enemies){
                if (bullet.collides(enemy) != null){
                    enemy.reduceLife(bullet.damage);
                    if (enemy.getLife() <= 0){
                        PlayingState.addScore(enemy.getType() * 5);
                        PlayingState.addCurrency(10 + (enemy.getType() * 5));
                        enemiesToRemove.add(enemy);
                    }
                    bulletsToRemove.add(bullet);
                }
            }
        }
        for (Bullet bullet : bulletsToRemove){
            bullets.remove(bullet);
        }
        for (Enemy enemy : enemiesToRemove){
            enemies.remove(enemy);
        }
    }

    private void createBullets(final int delta){
        double closest;
        for (DefenseTower dt : PlayingState.defenseTower){
            closest = Double.POSITIVE_INFINITY;
            dt.update(delta);
            for (Enemy enemy : enemies){
                Vector distance = new Vector(dt.getPosition().getX() - enemy.getPosition().getX(), dt.getPosition().getY() - enemy.getPosition().getY());
                if (distance.length() < closest){
                    closest = distance.length();
                    dt.gun.setRotation(distance.getRotation() - 180);
                }
                if (!dt.reloading) {
                    if (distance.length() < 160) {
                        dt.reloading = true;
                        bullets.add(new Bullet((int) dt.getPosition().getX(), (int) dt.getPosition().getY(), -distance.unit().scale(.4f).getX(), -distance.unit().scale(.4f).getY(), dt.towerType, velocityMultiplier));
                    }
                }
            }
        }
    }

    @Override
    public int getID(){
        return Game.ENEMYWAVESTATE;
    }
}

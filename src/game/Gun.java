package game;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class Gun extends Entity {

    Vector direction;

    Gun(int x, int y, final float dx, final float dy, int gunType){
        super(x, y);
        direction = new Vector(dx, dy);
        if (gunType == 0) {
            addImage(ResourceManager.getImage(Game.TOWER_1_GUN_RSC));
            scale(.6f);
        }
        else if (gunType == 1) {
            addImage(ResourceManager.getImage(Game.TOWER_2_GUN_RSC));
            scale(.7f);
        }
        else if (gunType == 2) {
            addImage(ResourceManager.getImage(Game.TOWER_3_GUN_RSC));
            scale(.7f);
        }
    }
}

package game;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class Bullet extends Entity {
    Vector velocity;
    int damage;

    Bullet(int x, int y, float vx, float vy, int type, float velocityMultiplier){
        super(x, y);
        damage = type + 1;
//        System.out.println("Bullet velocity: (" + vx + ", " + vy + ")");
        velocity = (new Vector(vx, vy)).scale(velocityMultiplier);
        addImageWithBoundingBox(ResourceManager.getImage(EnemyWaveState.bulletRSC[type]));
    }

    public void update(final int delta){
        translate(velocity.scale(delta));
    }

    public void fastForward(float multiplier){
        velocity = velocity.scale(multiplier);
    }

    public void disableFastForward(float multiplier){
        velocity = velocity.scale(multiplier);
    }
}
